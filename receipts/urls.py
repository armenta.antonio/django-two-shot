from django.urls import path
from receipts.views import (
    ReceiptListView,
    NewAccountCreateView,
    NewCategoryCreateView,
    NewReceiptCreateView,
    AccountListView,
    CategoryListView,
)


urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path(
        "accounts/create",
        NewAccountCreateView.as_view(),
        name="create_account",
    ),
    path(
        "categories/create",
        NewCategoryCreateView.as_view(),
        name="create_category",
    ),
    path("create/", NewReceiptCreateView.as_view(), name="create_receipt"),
    path("accounts/", AccountListView.as_view(), name="accounts_list"),
    path("categories/", CategoryListView.as_view(), name="categories_list"),
]
